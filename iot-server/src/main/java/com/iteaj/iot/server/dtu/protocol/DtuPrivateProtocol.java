package com.iteaj.iot.server.dtu.protocol;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.dtu.DtuCommonProtocolType;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dtu的私有协议
 */
public class DtuPrivateProtocol extends ClientInitiativeProtocol<ServerMessage> {

    public DtuPrivateProtocol(ServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected ServerMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    protected void doBuildRequestMessage(ServerMessage requestMessage) {
        if(logger.isDebugEnabled()) {
            logger.debug("DTU设备 私有协议 - 设备编号: {} - 协议类型: {} - 报文: {}"
                    , requestMessage.getHead().getEquipCode(), protocolType(), requestMessage);
        }
    }

    @Override
    public ProtocolType protocolType() {
        return DtuCommonProtocolType.DTU;
    }
}
