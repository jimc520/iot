package com.iteaj.iot.server.dtu.message;

import com.iteaj.iot.Message;
import com.iteaj.iot.ProtocolType;

public interface DtuMessage extends Message {

    String getEquipCode();

    void setEquipCode(String equipCode);

    ProtocolType getProtocolType();

    DtuMessage setProtocolType(ProtocolType type);

}
