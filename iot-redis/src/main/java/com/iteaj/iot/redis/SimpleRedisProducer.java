package com.iteaj.iot.redis;

import com.iteaj.iot.redis.producer.RedisProducer;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.Protocol;

/**
 * @see AbstractProtocol
 * 一种协议只支持单个Key
 * @param <T>
 */
public interface SimpleRedisProducer<T extends Protocol, O> extends RedisProducer<T> {

    /**
     * 返回 redis key
     * @return
     */
    String getKey();

    O operation();
}
