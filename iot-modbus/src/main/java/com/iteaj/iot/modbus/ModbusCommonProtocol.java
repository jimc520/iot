package com.iteaj.iot.modbus;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.modbus.consts.ModbusCode;

/**
 * 居于iot框架实现的通用的Modbus操作协议
 */
public interface ModbusCommonProtocol extends Protocol {

    Payload getPayload();

    /**
     * @see ModbusCode or other
     * @return
     */
    ProtocolType protocolType();
}
